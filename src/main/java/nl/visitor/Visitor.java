package nl.visitor;

public interface Visitor {

    void visit();
}
