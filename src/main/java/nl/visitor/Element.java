package nl.visitor;

public interface Element {

    void accept(Visitor visitor);
}
